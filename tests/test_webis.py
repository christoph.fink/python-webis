#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas
import pandas.util.testing
import unittest

from context import webis


class TestCyclistTweets(unittest.TestCase):
    def setUp(self):
        self.tweets_input_list = [
            (
                1,
                "What a beautiful morning! There’s nothing better than "
                + "cycling to work on a sunny day 🚲."
            ),
            (
                2,
                "Argh, I hate it when you find seven (7!) cars blocking "
                + "the bike lane on a five-mile commute"
            )
        ]

        self.tweets_input_pandasdf = pandas.DataFrame([
            (
                1,
                "What a beautiful morning! There’s nothing better than "
                + "cycling to work on a sunny day 🚲."
            ),
            (
                2,
                "Argh, I hate it when you find seven (7!) cars blocking "
                + "the bike lane on a five-mile commute"
            )
        ])

        self.tweets_input_dict = {
            1:
                "What a beautiful morning! There’s nothing better than "
                + "cycling to work on a sunny day 🚲.",
            2:
                "Argh, I hate it when you find seven (7!) cars blocking "
                + "the bike lane on a five-mile commute"
        }

        self.expected_output_list = [
            (1, "positive"),
            (2, "negative")
        ]

        self.expected_output_pandasdf = pandas.DataFrame({
            "sentiment": ["positive", "negative"],
            "tweetId": [1, 2]
        })

        self.expected_output_dict = {
            1: "positive",
            2: "negative"
        }

        self.sentimentIdentifier = webis.SentimentIdentifier()

    def test_cyclists_list(self):
        self.output = \
            self.sentimentIdentifier.identifySentiment(
                self.tweets_input_list
            )
        self.assertEqual(
            self.output,
            self.expected_output_list
        )

    def test_cyclists_pandasdf(self):
        self.output = \
            self.sentimentIdentifier.identifySentiment(
                self.tweets_input_pandasdf
            )
        pandas.util.testing.assert_frame_equal(
            self.output,
            self.expected_output_pandasdf
        )

    def test_cyclists_dict(self):
        self.output = \
            self.sentimentIdentifier.identifySentiment(
                self.tweets_input_dict
            )
        self.assertEqual(
            self.output,
            self.expected_output_dict
        )
